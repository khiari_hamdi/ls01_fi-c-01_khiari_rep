import java.io.PrintStream;
public class Fakultaet {
    public static void main(String[] args){
        //0
        System.out.printf("0!%5s=1"," ");
        System.out.printf("%14s="," ");
        System.out.printf("%4s1%n"," ");
        //1
        System.out.printf("1!%5s=1"," ");
        System.out.printf("%14s="," ");
        System.out.printf("%4s1%n"," ");
        //2
        System.out.printf("2!%5s=1*2"," ");
        System.out.printf("%12s="," ");
        System.out.printf("%4s2%n"," ");
        //3
        System.out.printf("3!%5s=1*2*3"," ");
        System.out.printf("%10s="," ");
        System.out.printf("%4s6%n"," ");
        //4
        System.out.printf("4!%5s=1*2*3*4"," ");
        System.out.printf("%8s="," ");
        System.out.printf("%3s24%n"," ");
        //5
        System.out.printf("5!%5s=1*2*3*4*5"," ");
        System.out.printf("%6s="," ");
        System.out.printf("%2s120%n"," ");

    }
    
}