﻿import java.util.Scanner;
import java.io.PrintStream;


class Fahrkartenautomat{
	
	
	//METHODEN------------------------------------------------------------------------------------------------------------------------------------------------
	
	//fahrkartenbestellungErfassen----------------------------------------------------------------------------------------------------------------------------
	static double fahrkartenbestellungErfassen() {
	       Scanner tastatur = new Scanner(System.in);
	      // Scanner fahrkartenanzahl = new Scanner(System.in);
	       System.out.println("Bitte geben Sie die zahl der gewünchten Fahrkarten ein :");
	       byte kartenAnzahl = tastatur.nextByte();
	       System.out.print("Zu zahlender Betrag (EURO): ");
	       double zuZahlenderBetrag = tastatur.nextDouble() * kartenAnzahl;
		   return zuZahlenderBetrag;
	}
	
	
	//fahrkartenBezahlen--------------------------------------------------------------------------------------------------------------------------------------
	static double fahrkartenBezahlen(double zuZahlenderBetrag) {

	       double eingezahlterGesamtbetrag;
	       double eingeworfeneMünze;

           
	       // Geldeinwurf
	       // -----------------------------------------------------------------------------------------------------------------------------------------------
	       Scanner tastatur = new Scanner(System.in);
	       
	       eingezahlterGesamtbetrag = 0.0;
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   System.out.printf("Noch zu zahlen: " + "%.2f%n" ,(zuZahlenderBetrag - eingezahlterGesamtbetrag));
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	   eingeworfeneMünze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneMünze;
	       }
	
	           
           
		return eingezahlterGesamtbetrag;
	       }

    // Rückgeldberechnung und -Ausgabe
    // ------------------------------------------------------------------------------------------------------------------------------------------------

    static void rückgabebetrag (double eingezahlterGesamtbetrag, double zuZahlenderBetrag ) {
	       double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       if(rückgabebetrag > 0.0)
	       {
	    	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	        	  System.out.println("2 EURO");
		          rückgabebetrag -= 2.0;
	           }
	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
		          rückgabebetrag -= 1.0;
	           }
	           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          rückgabebetrag -= 0.5;
	           }
	           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          rückgabebetrag -= 0.2;
	           }
	           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          rückgabebetrag -= 0.1;
	           }
	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	 	          rückgabebetrag -= 0.05;
	           }


	       }
    }

	
	//Fahrkrtenausgabe----------------------------------------------------------------------------------------------------------------------------------------
	static void fahrkartenausgabe(){
	       System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
		try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}


		System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir wünschen Ihnen eine gute Fahrt.");
	    }

	//MAIN METHODE
    public static void main(String[] args)
    {

       double zuZahlenderBetrag; 
       double rückgabebetrag;
       double eingezahlterGesamtbetrag=0;


       
     //Aufrufe------------------------------------------------------------------------------------------------------------------------------------------------
       zuZahlenderBetrag = fahrkartenbestellungErfassen();
       rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
       fahrkartenausgabe();
       rückgabebetrag(eingezahlterGesamtbetrag,zuZahlenderBetrag);

    }
}

  